package week5.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods01;
import wdMethods.ProjectMethodsOld;

public class TC002_EditLead extends ProjectMethodsOld{
	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="Bharath";		
	}
	
	@Test
	public void editLead() throws InterruptedException{
		/*
		 * driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Bharath");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000); 
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		driver.findElementByLinkText("Edit").click();
		Thread.sleep(1000);
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Apple");
		driver.findElementByName("submitButton").click();
		 
		 */
	/*WebElement eleSubmit=locateElement("class", "decorativeSubmit");
	click(eleSubmit);
	WebElement eleCRMSFA=locateElement("linkText", "CRM/SFA");
	click(eleCRMSFA);
	*/
	WebElement eleLeads=locateElement("linkText","Leads");
	click(eleLeads);
	Thread.sleep(1000);
	WebElement eleFindLeads=locateElement("linkText", "Find Leads");
	click(eleFindLeads);
	WebElement eleFirstName=locateElement("xpath", "(//input[@name='firstName'])[3]");
	type(eleFirstName, "Bharath");
	WebElement eleButtonClick=locateElement("xpath", "//button[text()='Find Leads']");
	click(eleButtonClick);
	Thread.sleep(3000);
	WebElement eleClick=locateElement("xpath", "(//a[@class='linktext'])[4]");
	click(eleClick);
	WebElement eleEditClick=locateElement("linkText", "Edit");
	click(eleEditClick);
	Thread.sleep(1000);
	WebElement eleClearCompanyName=locateElement("id", "updateLeadForm_companyName");
	type(eleClearCompanyName, "");
	type(eleClearCompanyName, "Apple");
	WebElement eleClickSubB=locateElement("name", "submitButton");
	click(eleClickSubB);
	
	}
}
