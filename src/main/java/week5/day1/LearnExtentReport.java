package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReport {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/results.html");
		html.setAppendExisting(true);
		ExtentReports extent =  new ExtentReports();
		extent.attachReporter(html);
		
		//TestCase Level
		ExtentTest test= extent.createTest("TC001_CreateLead", "Create an new Lead in leafstaps");
		test.assignCategory("SmokeTest");
		test.assignAuthor("Bharath");
		
		//TestCase Step Level
		
		test.pass("Browser Launched Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		test.fail("screen not displayed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		extent.flush();
		
		
		

	}

}
