package week5.day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.CardanEulerSingularityException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {

	public static void main(String[] args) throws InterruptedException {
		
		//without selmethods
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.findElementByClassName("search").click();
		driver.findElementByClassName("items").click();
		driver.findElementByClassName("proceed").click();
		
		// Get the current date
				Date date = new Date();
		// Get only the date (and not month, year, time etc)
				DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
				String today = sdf.format(date);
		// Convert to integer and add 1 to it
				int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
				System.out.println(tomorrow);
		
		//day picked full		
				List<WebElement> allDays =driver.findElementsByXPath("//div[@class='day']");
				System.out.println(allDays.size());
				
				for (WebElement day : allDays) {
					if(day.getText().contains(Integer.toString(tomorrow))){
						System.out.println(day.getText());
						day.click();		
					}
				}
				driver.findElementByClassName("proceed").click();
				driver.findElementByClassName("proceed").click();
				Thread.sleep(10000);
				//List<WebElement> carListing=driver.findElementsByXPath("//div[@class='price']");
				List<WebElement> priceTags=driver.findElementsByXPath("//div[@class='price']");
				System.out.println(priceTags.size());
				
				
				List<String> priceList=new ArrayList<String>();
									 
				for (WebElement price : priceTags) {
					//
					priceList.add(price.getText().trim().replaceAll("\\D",""));
					//System.out.println(priceList);
														
				}
				System.out.println(priceList);
				Collections.sort(priceList);
				int size=priceList.size();
				String max=priceList.get(size-1);
				String Brand = driver.findElementByXPath("//div[text()[contains(.,'"+max+"')]]/../../..//following::div/h3").getText();
				System.out.println("BrandName: "+Brand);
				driver.findElementByXPath("//div[text()[contains(.,'"+max+"')]]/following::button").click();		
	}
}
