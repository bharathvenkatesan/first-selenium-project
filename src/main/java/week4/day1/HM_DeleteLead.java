package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class HM_DeleteLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//span[text()='Phone']").click();
		driver.findElementByName("phoneNumber").sendKeys("999999998");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		String leadid = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		System.out.println("Lead ID of First Resulting Lead: " +leadid);
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		
		//delete
		driver.findElementByClassName("subMenuButtonDangerous").click();
		System.out.println("Deleted");
		
		/*
		driver.findElementByLinkText("Find Leads").click();
		
		driver.findElementByXPath("(//input[@name='id']").sendKeys(leadid);
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		*/

	}

}
