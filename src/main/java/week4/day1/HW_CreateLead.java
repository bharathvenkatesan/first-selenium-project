package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HW_CreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("TCS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Bharath");
		driver.findElementById("createLeadForm_lastName").sendKeys("V");
		
		//return type of findelement is WebElement and hence WeblElement src = 
		WebElement src =driver.findElementById("createLeadForm_dataSourceId");
		//selecting drop down
		Select dropDown = new Select(src);
		//
		dropDown.selectByVisibleText("Direct Mail");
		
		WebElement src1=driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropDown1 = new Select(src1);
		// all options in the dropdown will be stored in list webelement "options"
		List<WebElement> options= dropDown1.getOptions();
		int size=options.size();
		dropDown1.selectByIndex(size-2);
		
		driver.findElementByName("submitButton").click();


	}

}
