package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowsSnapshot {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
		
		//Window Handle
		
		Set<String> allwindows =driver.getWindowHandles();
		List<String> listOfWindows = new ArrayList<>();
		listOfWindows.addAll(allwindows);
		driver.switchTo().window(listOfWindows.get(1));
		System.out.println("Title of 2nd Window: "+driver.getTitle());
		System.out.println("Currnet Url of 2nd Window: "+driver.getCurrentUrl());
		
		//Snapshot
		File scr = driver.getScreenshotAs(OutputType.FILE);
		File dcr = new File("./snaps/img.png");
		FileUtils.copyFile(scr, dcr);
		
		driver.switchTo().window(listOfWindows.get(0));
		System.out.println("First Windown: "+driver.getTitle());
		driver.close();
		
		
		
		
		

	}

}
