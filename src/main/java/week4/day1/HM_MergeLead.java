package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class HM_MergeLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//img[@src='/images/fieldlookup.gif']").click();
		
		//windows
		
		Set<String> allwindows =driver.getWindowHandles();
		List<String> listOfWindows = new ArrayList<>();
		listOfWindows.addAll(allwindows);
		driver.switchTo().window(listOfWindows.get(1));
		System.out.println("Title of 2nd Window: "+driver.getTitle());
		System.out.println("Currnet Url of 2nd Window: "+driver.getCurrentUrl());
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@name='id']").sendKeys("10840");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		
		driver.findElementByXPath("//a[@class='linktext']").click();
		driver.switchTo().window(listOfWindows.get(0));
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		driver.switchTo().window(listOfWindows.get(1));
		driver.findElementByXPath("//input[@name='id']").sendKeys("10860");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//a[@class='linktext']").click();
		driver.switchTo().window(listOfWindows.get(0));
		driver.findElementByLinkText("Merge").click();
		System.out.println("Merged");
		//10840
		//10860
		

	}

}
