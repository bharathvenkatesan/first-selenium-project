package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods_old.SeMethods;

public class TC004_DeleteLead extends SeMethods{

@Test	
public void deletelead() throws InterruptedException {

startApp("chrome", "http://leaftaps.com/opentaps");
WebElement eleUserName= locateElement("id", "username");
type(eleUserName, "demosalesmanager");

WebElement elePassword = locateElement("id", "password");
type(elePassword, "crmsfa");

WebElement eleSubmit = locateElement("class", "decorativeSubmit");
click(eleSubmit);
/*
 		Thread.sleep(2000);
		String leadid = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		System.out.println("Lead ID of First Resulting Lead: " +leadid);
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		
		//delete
		driver.findElementByClassName("subMenuButtonDangerous").click();
		System.out.println("Deleted");
 */

WebElement eleCrmsfa = locateElement("linktext", "CRM/SFA");
click(eleCrmsfa);

WebElement eleLeads = locateElement("linktext", "Leads");
click(eleLeads);

WebElement eleFindLeads = locateElement("linktext", "Find Leads");
click(eleFindLeads);

WebElement elePhone = locateElement("xpath", "//span[text()='Phone']");
click(elePhone);

WebElement eleTypePhoneNo = locateElement("name", "phoneNumber");
type(eleTypePhoneNo, "9999888877");

WebElement eleFindLeadClick = locateElement("xpath", "//button[text()='Find Leads']");
click(eleFindLeadClick);
Thread.sleep(20000);

WebElement eleGetText = locateElement("xpath", "(//a[@class='linktext'])[4]");
String text = getText(eleGetText);
System.out.println("Lead id to be deleted:" +text);

WebElement eleSelectFirstLead = locateElement("xpath", "(//a[@class='linktext'])[4]");
click(eleSelectFirstLead);
/*
driver.findElementByClassName("subMenuButtonDangerous").click();
System.out.println("Deleted");
*/
WebElement eleClickDelete = locateElement("class", "subMenuButtonDangerous");
click(eleClickDelete);
System.out.println("Deleted");

}
}
