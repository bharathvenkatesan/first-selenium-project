package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods_old.SeMethods;

public class TC002_CreateLead extends SeMethods {
	
	@Test
	public void createLead() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUserName = locateElement("id", "username");	
	type(eleUserName, "DemoSalesManager");
	WebElement elePassWord = locateElement("id", "password");
	type(elePassWord, "crmsfa");
	
	//login button click
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	
	WebElement eleCRMSFA = locateElement("linktext", "CRM/SFA");
	click(eleCRMSFA);
	
	WebElement eleCreateLead = locateElement("linktext", "Create Lead");
	click(eleCreateLead);
	
	WebElement eleCompanyName = locateElement("id","createLeadForm_companyName");
	type(eleCompanyName, "TCS");
	
	WebElement elecFirstName = locateElement("id","createLeadForm_firstName");
	type(elecFirstName, "Bharath");
	
	WebElement elecLastName = locateElement("id","createLeadForm_lastName");
	type(elecLastName, "V");
	
	WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(eleSource, "Employee");
	
	WebElement eleSaluation= locateElement("id", "createLeadForm_personalTitle");
	type(eleSaluation, "Tester");
	
	WebElement eleTitle= locateElement("id", "createLeadForm_generalProfTitle");
	type(eleTitle, "Engineer");
	
	WebElement eleRevenue= locateElement("id", "createLeadForm_annualRevenue");
	type(eleRevenue, "100000");
	
	WebElement eleIndustry = locateElement("id", "createLeadForm_industryEnumId");
	selectDropDownUsingIndex(eleIndustry, 1);
	
	WebElement eleOwnership = locateElement("id", "createLeadForm_ownershipEnumId");
	selectDropDownUsingIndex(eleOwnership, 1);
	
	WebElement eleSIC= locateElement("id", "createLeadForm_sicCode");
	type(eleSIC, "66666");
	
	WebElement eleDescription= locateElement("id", "createLeadForm_description");
	type(eleDescription, "Testing is in progress");
	
	WebElement eleImp= locateElement("id", "createLeadForm_importantNote");
	type(eleImp, "Important Note: Testing is in progress");
	
	WebElement elePhone= locateElement("id", "createLeadForm_primaryPhoneNumber");
	type(elePhone, "9999888877");
	
	WebElement eleSubmit=locateElement("name", "submitButton");
	click(eleSubmit);
	/*
	WebElement eleFirstName=locateElement("id", "viewLead_firstName_sp");
	String retrievedText=getText(eleFirstName);
	verifyDisplayed(eleFirstName);
	*/
	
	
}

}
