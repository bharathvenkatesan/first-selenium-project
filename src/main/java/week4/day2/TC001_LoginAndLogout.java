package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods_old.SeMethods;

public class TC001_LoginAndLogout extends SeMethods{
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");	
		type(eleUserName, "DemoSalesManager");
		WebElement elePassWord = locateElement("id", "password");
		type(elePassWord, "crmsfa");
		
		//login button click
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleLogOut = locateElement("class", "decorativeSubmit");
		click(eleLogOut);
	
	}
		

}
