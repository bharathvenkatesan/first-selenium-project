package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
///
//***********this project method is before implementing excel*****

////
public class ProjectMethods01 extends SeMethods{
	@BeforeSuite(groups="common")
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups="common")
	public void beforeClass() {
		startTestCase();
	}
	/*
	@BeforeMethod(groups="common")
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);*/
	 
	//for parameter 22 sep 18
	
	@BeforeMethod(groups= {"common"})
	@Parameters({"url","username","password"})
	public void login(String url, String username, String password) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
	}
	@AfterMethod(groups="common")
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups="common")
	public void afterSuite() {
		endResult();
	}
	
	
	
	
	
	
	
	
}
