package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		List<String> myPhones = new ArrayList<String>();
		myPhones.add("Nokia");
		myPhones.add("Samsung");
		myPhones.add("Iphone");
		myPhones.add("Samsung");
		myPhones.add("Moto");
		
		System.out.println("Before Sorting");
		System.out.println("------------------------");
		for (String eachPhones : myPhones) {
			System.out.println(eachPhones);
		}
		int size=myPhones.size();
		System.out.println("------------------------");
		System.out.println("Count of Mobiles : "+ size);
		String lastPhone=myPhones.get(size-2);
		System.out.println("Last but one Mobile Name: " + lastPhone);
		Collections.sort(myPhones);
		System.out.println("Ascii Ordered List");
		System.out.println("------------------------");
		for (String eachPhones : myPhones) {
			System.out.println(eachPhones);

	}
	}
}
