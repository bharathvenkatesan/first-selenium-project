package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomeWork {

	public static void main(String[] args) {
		
     /*
      In a given string "Amazon India"
      1. Print the characters in sorted order
      2. Print the characters in reverse sorted order with case sensitivity.
      (to lower case or upper case)
      3. Remove the duplicate and print in sorted order.  
      */
		
		String text="Amazon India";
		int count=0;
		char[] allChar =text.toCharArray();

		for (char ch : allChar) 
		{
			System.out.println(ch);
		}
		
		List<Character> eachChar = new ArrayList<Character>();
		for (char ch : allChar) {
			eachChar.add(ch);
			
		}
		
		Collections.sort(eachChar);
		
		for (char sortedChar : eachChar) {
			System.out.println("After Sorting: "  + sortedChar);
		}		
		
	}

}
