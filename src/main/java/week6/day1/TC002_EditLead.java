package week6.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods01;

public class TC002_EditLead extends ProjectMethods01{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC002_EditLead";
		testCaseDesc="Edit a Lead";
		category="smoke";
		author="Bharath";		
	}
	
	@Test(dependsOnMethods= {"week6.day1.TC001_CreateLead.createLead"},enabled=false)
	public static void main(String[] args) {
		System.out.println("Edit Lead Pass");
	}
	
	}
