package week6.day1.classwork;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods01;
import wdMethods_old.SeMethods;

public class TC003_DeleteLead extends ProjectMethods01{

	@BeforeTest(groups="sanity")
	public void setData() {
		testCaseName="TC002_DeleteLead";
		testCaseDesc="Delete a new Lead";
		category="sanity";
		author="Bharath";		
	}
	
@Test(groups="sanity",dependsOnGroups= {"smoke"})
public void deletelead() throws InterruptedException {



WebElement eleLeads = locateElement("linkText", "Leads");
click(eleLeads);

WebElement eleFindLeads = locateElement("linkText", "Find Leads");
click(eleFindLeads);

WebElement elePhone = locateElement("xpath", "//span[text()='Phone']");
click(elePhone);

WebElement eleTypePhoneNo = locateElement("name", "phoneNumber");
type(eleTypePhoneNo, "9999888877");

WebElement eleFindLeadClick = locateElement("xpath", "//button[text()='Find Leads']");
click(eleFindLeadClick);
Thread.sleep(20000);

WebElement eleGetText = locateElement("xpath", "(//a[@class='linktext'])[4]");
String text = getText(eleGetText);
System.out.println("Lead id to be deleted:" +text);

WebElement eleSelectFirstLead = locateElement("xpath", "(//a[@class='linktext'])[4]");
click(eleSelectFirstLead);
/*
driver.findElementByClassName("subMenuButtonDangerous").click();
System.out.println("Deleted");
*/
WebElement eleClickDelete = locateElement("class", "subMenuButtonDangerous");
click(eleClickDelete);
System.out.println("Deleted");

}
}
