package week6.day1.classwork;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods01;

public class TC002_EditLead extends ProjectMethods01{
	
	@BeforeTest(groups="sanity")
	public void setData() {
		testCaseName="TC002_EditLead";
		testCaseDesc="Edit a Lead";
		category="smoke";
		author="Bharath";		
	}
	
	@Test(groups="sanity", dependsOnGroups= {"smoke"})
	public  void main() {
		System.out.println("Edit Lead Pass");
	}
	
	}
