package week6.day1.classworkDataProvider;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods01;
import wdMethods.ProjectMethodsOld;

public class TC002_EditLead extends ProjectMethods01{
	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="Bharath";		
	}
	
	@Test(groups="sanity", dataProvider="positive")
	public void editLead(String fname, String cname) throws InterruptedException{
		/* *** start Before SeMethod, ProjectMethod ****
		 * 
		 * driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Bharath");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000); 
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		driver.findElementByLinkText("Edit").click();
		Thread.sleep(1000);
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Apple");
		driver.findElementByName("submitButton").click();
		
		*** End Before SeMethod, ProjectMethod ****
		*/
		
		/* Start After SeMethod, ProjectMethod ****
	WebElement eleSubmit=locateElement("class", "decorativeSubmit");
	click(eleSubmit);
	WebElement eleCRMSFA=locateElement("linktext", "CRM/SFA");
	click(eleCRMSFA);
	WebElement eleLeads=locateElement("linktext","Leads");
	click(eleLeads);
	WebElement eleFindLeads=locateElement("linktext", "Find Lead");
	click(eleFindLeads);
	WebElement eleFirstName=locateElement("xpath", "(//input[@name='firstName'])[3]");
	type(eleFirstName, "Bharath");
	WebElement eleButtonClick=locateElement("xpath", "//button[text()='Find Leads']");
	click(eleButtonClick);
	Thread.sleep(3000);
	WebElement eleClick=locateElement("xpath", "(//a[@class='linktext'])[4]");
	click(eleClick);
	WebElement eleEditClick=locateElement("linktext", "Edit");
	click(eleEditClick);
	Thread.sleep(1000);
	WebElement eleClearCompanyName=locateElement("id", "updateLeadForm_companyName");
	type(eleClearCompanyName, "");
	type(eleClearCompanyName, "Apple");
	WebElement eleClickSubB=locateElement("name", "submitButton");
	click(eleClickSubB);
	Start After SeMethod, ProjectMethod ****
	*/
	
		// Start After Data Provider ****
		/*WebElement eleSubmit=locateElement("class", "decorativeSubmit");
		click(eleSubmit);
		WebElement eleCRMSFA=locateElement("linktext", "CRM/SFA");
		click(eleCRMSFA);
		*/
		WebElement eleLeads=locateElement("linkText","Leads");
		click(eleLeads);
		Thread.sleep(1000);
		WebElement eleFindLeads=locateElement("linkText","Find Leads");
		click(eleFindLeads);
		WebElement eleFirstName=locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, fname);
		WebElement eleButtonClick=locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButtonClick);
		Thread.sleep(3000);
		WebElement eleClick=locateElement("xpath", "(//a[@class='linktext'])[4]");
		click(eleClick);
		WebElement eleEditClick=locateElement("linkText", "Edit");
		click(eleEditClick);
		Thread.sleep(1000);
		WebElement eleClearCompanyName=locateElement("id", "updateLeadForm_companyName");
		type(eleClearCompanyName, "");
		type(eleClearCompanyName, cname);
		WebElement eleClickSubB=locateElement("name", "submitButton");
		click(eleClickSubB);
		//End Data Provider ****
		
	}
	
	@DataProvider(name="positive")
	public Object[][] fetchData(){
		Object[][] data = new Object[2][2];
		data[0][0]="Bharath";
		data[0][1]="Apple";
				
		data[1][0]="Bharath";
		data[1][1]="Google";
				
		return data;
	}
}
