package week6.day2.excel;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.ProjectMethods01;
import wdMethods_old.SeMethods;

public class TC001_CreateLead extends ProjectMethods {
	@BeforeTest(groups="smoke")
	public void setData() {
		testCaseName="TC001_CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="Bharath";
		filename="CL";
	}
	
	
	@Test(groups="smoke", dataProvider="positive")
	public void createLead(String cname, String fname, String lname) {
	
	
	WebElement eleCreateLead = locateElement("linkText", "Create Lead");
	click(eleCreateLead);
	
	WebElement eleCompanyName = locateElement("id","createLeadForm_companyName");
	type(eleCompanyName, cname);
	
	WebElement elecFirstName = locateElement("id","createLeadForm_firstName");
	type(elecFirstName, fname);
	
	WebElement elecLastName = locateElement("id","createLeadForm_lastName");
	type(elecLastName, lname);
	
	WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(eleSource, "Employee");
	
	WebElement eleSaluation= locateElement("id", "createLeadForm_personalTitle");
	type(eleSaluation, "Tester");
	
	WebElement eleTitle= locateElement("id", "createLeadForm_generalProfTitle");
	type(eleTitle, "Engineer");
	
	WebElement eleRevenue= locateElement("id", "createLeadForm_annualRevenue");
	type(eleRevenue, "100000");
	
	WebElement eleIndustry = locateElement("id", "createLeadForm_industryEnumId");
	selectDropDownUsingIndex(eleIndustry, 1);
	
	WebElement eleOwnership = locateElement("id", "createLeadForm_ownershipEnumId");
	selectDropDownUsingIndex(eleOwnership, 1);
	
	WebElement eleSIC= locateElement("id", "createLeadForm_sicCode");
	type(eleSIC, "66666");
	
	WebElement eleDescription= locateElement("id", "createLeadForm_description");
	type(eleDescription, "Testing is in progress");
	
	WebElement eleImp= locateElement("id", "createLeadForm_importantNote");
	type(eleImp, "Important Note: Testing is in progress");
	
	WebElement elePhone= locateElement("id", "createLeadForm_primaryPhoneNumber");
	type(elePhone, "9999888877");
	
	WebElement eleSubmit=locateElement("name", "submitButton");
	click(eleSubmit);
	/*
	WebElement eleFirstName=locateElement("id", "viewLead_firstName_sp");
	String retrievedText=getText(eleFirstName);
	verifyDisplayed(eleFirstName);
	*/
	
	

}
	/*@DataProvider(name="positive")
	public Object[][] fetchData(){
		return ReadExcel.getExcelData(filename);
		*/
	
	
	
	
}
