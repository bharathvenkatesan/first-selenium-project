package week6.day2.excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String filename) throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row Count= "+rowCount);
		int columnCount=sheet.getRow(0).getLastCellNum();
		System.out.println("Column Count= "+columnCount);
		Object[][] data = new Object[rowCount][columnCount];
		for(int j=1;j<=rowCount;j++) {
			XSSFRow row=sheet.getRow(j);
			for(int i=0;i<columnCount;i++) {
				XSSFCell cell= row.getCell(i);
				data[j-1][i]=cell.getStringCellValue();
			//	String cellValue= cell.getStringCellValue();
			//	System.out.println(cellValue);
			}
			
		}
		return data;
		
	/*	
		XSSFRow row = sheet.getRow(0);
		XSSFCell column = row.getCell(1);
		*/
		
		
		
		
	}

}
