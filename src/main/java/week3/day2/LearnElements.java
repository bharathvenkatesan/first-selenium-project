package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/table.html");
		List<WebElement> allCheckBox = driver.findElementsByXPath("(//input[@type='checkbox'])");
		int size = allCheckBox.size();
		System.out.println("Total Number of Check box: "+size);
		allCheckBox.get(size-1).click();
		
	}

}
