package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.findElementById("username").sendKeys("DemoSalesManager");
		} catch (NoSuchElementException e) {
			System.out.println("No Such Element Found");
			// if you use throw new RuntimeException then below script(entering password) wont exeucte
			//if you dont use throw, error msg displayed, below script (entering password) will execute
			//throw new RuntimeException();
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		/*
		 * 
		 * finally {
			driver.close();
		}*/
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("TCS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Bharath");
		driver.findElementById("createLeadForm_lastName").sendKeys("V");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("999999998");
		
		//return type of findelement is WebElement and hence WeblElement src = 
		WebElement src =driver.findElementById("createLeadForm_dataSourceId");
		//selecting drop down
		Select dropDown = new Select(src);
		//
		dropDown.selectByVisibleText("Direct Mail");
		
		WebElement src1=driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropDown1 = new Select(src1);
		// all options in the dropdown will be stored in list webelement "options"
		List<WebElement> options= dropDown1.getOptions();
		int size=options.size();
		dropDown1.selectByIndex(size-2);
		
		driver.findElementByName("submitButton").click();
		
		
	}
	

}
