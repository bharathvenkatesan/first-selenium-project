package week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("BharVeny321");
		driver.findElementById("userRegistrationForm:password").sendKeys("BharVeny321");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("BharVeny321");
		
		WebElement src =driver.findElementById("userRegistrationForm:securityQ");
		Select dropDown = new Select(src);
		dropDown.selectByVisibleText("What is your pet name?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Fish");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement src1 =driver.findElementById("userRegistrationForm:prelan");
		Select dropDown1 = new Select(src);
		dropDown1.selectByVisibleText("English");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Bharath");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		WebElement dob =driver.findElementById("userRegistrationForm:dobDay");
		Select birth = new Select(dob);
		birth.selectByVisibleText("01");
		
		WebElement mob =driver.findElementById("userRegistrationForm:dobMonth");
		Select month = new Select(mob);
		month.selectByVisibleText("JAN");
		
		WebElement yob =driver.findElementById("userRegistrationForm:dateOfBirth");
		Select year = new Select(yob);
		year.selectByVisibleText("JAN");
		
		WebElement occ =driver.findElementById("userRegistrationForm:occupation");
		Select occupation = new Select(occ);
		occupation.selectByVisibleText("JAN");
		
		WebElement cou =driver.findElementById("userRegistrationForm:countries");
		Select country = new Select(cou);
		country.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("test@testmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("999999999");
		driver.findElementById("userRegistrationForm:nationalityId").sendKeys("");
		
		WebElement nat =driver.findElementById("userRegistrationForm:nationalityId");
		Select nation = new Select(nat);
		nation.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("25, 1st Street, Chennai");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("602001");
		
		
		

	}

}
